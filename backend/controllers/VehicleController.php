<?php

namespace backend\controllers;

use common\exceptions\BaseException;
use common\helpers\{ArrayHelper, OutputHelper, PdfHelper, StorageHelper, view\VehicleHelper};
use common\exceptions\VehicleBuildingValidationException;
use common\helpers\view\AlertViewHelper;
use common\modules\BuildingVehicleStatus\Exceptions\VehicleTypeOverflowingException;
use common\models\address\{Country, Province};
use common\models\building\{Building, BuildingRule, BuildingVehicleStatus};
use common\models\MetaValue;
use common\models\parkingpermit\{ParkingPermit, ParkingPermitNumber, ParkingPermitVehicles};
use common\models\system\AdditionalMeta;
use common\models\tracker\{TrackerIncident, VehicleTracker};
use common\models\unit\Unit;
use common\models\user\{User, UserFile, UserFileVehicle, UserUnit};
use common\models\user\UserAuthToken;
use common\models\vehicle\{Vehicle,
    VehicleFile,
    VehicleMake,
    VehicleModel,
    VehicleOwner,
    VehicleSearchHistory,
    VehicleType};
use common\models\violation\{Violation, ViolationVehicle};
use common\modules\BuildingConsent\Services\BuildingConsentServiceInterface;
use common\modules\BuildingVehicleStatus\Services\BuildingVehicleStatusServiceInterface;
use common\modules\ParkingPermit\Services\ParkingPermitServiceInterface;
use common\modules\UserFile\Services\UserFileServiceInterface;
use common\modules\Vehicle\Exceptions\StoreVehicleException;
use common\modules\Vehicle\Exceptions\StoreVehiclePaymentException;
use common\modules\Vehicle\Services\VehicleServiceInterface;
use common\services\CookieService;
use common\services\Permission\PermissionServiceInterface;
use kartik\mpdf\Pdf;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\{Json, StringHelper};
use yii\web\{Application, BadRequestHttpException, ForbiddenHttpException, NotFoundHttpException, Response};

/**
 * VehicleController implements the CRUD actions for Vehicle model.
 */
class VehicleController extends BaseController
{
    /**
     * @var string
     */
    protected const MESSAGE_VEHICLE_REGISTRATION_IS_SUSPENDED = '<strong>Something went wrong!</strong> Vehicle Registration is suspended please contact your parking administrator.';

    /**
     * @var \common\modules\BuildingConsent\Services\BuildingConsentServiceInterface|\common\modules\BuildingRule\Services\BuildingRuleServiceInterface
     */
    protected BuildingConsentServiceInterface $buildingConsentService;

    /**
     * @var \common\modules\UserFile\Services\UserFileServiceInterface
     */
    protected UserFileServiceInterface $userFileService;

    /**
     * @var \common\modules\ParkingPermit\Services\ParkingPermitServiceInterface
     */
    protected ParkingPermitServiceInterface $parkingPermitService;

    /**
     * @var \common\services\Permission\PermissionServiceInterface
     */
    protected PermissionServiceInterface $permissionService;

    /**
     * @var \common\modules\Vehicle\Services\VehicleServiceInterface
     */
    protected VehicleServiceInterface $vehicleService;

    /**
     * @var \common\modules\BuildingVehicleStatus\Services\BuildingVehicleStatusServiceInterface
     */
    protected BuildingVehicleStatusServiceInterface $buildingVehicleStatusService;

    /**
     * @var \common\services\CookieService
     */
    protected CookieService $cookieService;

    /**
     * VehicleController constructor.
     *
     * @param string $id
     * @param Application $module
     * @param \common\modules\BuildingConsent\Services\BuildingConsentServiceInterface $buildingConsentService
     * @param \common\modules\UserFile\Services\UserFileServiceInterface $userFileService
     * @param \common\modules\ParkingPermit\Services\ParkingPermitServiceInterface $parkingPermitService
     * @param \common\services\Permission\PermissionServiceInterface $permissionService
     * @param \common\modules\Vehicle\Services\VehicleServiceInterface $vehicleService
     * @param \common\modules\BuildingVehicleStatus\Services\BuildingVehicleStatusServiceInterface $buildingVehicleStatusService
     */
    public function __construct(
        string                                $id,
        Application                           $module,
        BuildingConsentServiceInterface       $buildingConsentService,
        UserFileServiceInterface              $userFileService,
        ParkingPermitServiceInterface         $parkingPermitService,
        PermissionServiceInterface            $permissionService,
        VehicleServiceInterface               $vehicleService,
        BuildingVehicleStatusServiceInterface $buildingVehicleStatusService
    )
    {
        parent::__construct($id, $module);

        $this->buildingConsentService = $buildingConsentService;
        $this->userFileService = $userFileService;
        $this->parkingPermitService = $parkingPermitService;
        $this->vehicleService = $vehicleService;
        $this->permissionService = $permissionService;
        $this->buildingVehicleStatusService = $buildingVehicleStatusService;
        $this->cookieService = new CookieService(
            $this->vehicleService->getSearchClass()
        );
        $this->cookieService->setSearchCookies();
    }

    /**
     * Lists all VehicleTracker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'showAll', 'showOwn', true);
        list($searchModel, $dataProvider) = $this->vehicleService->getDataProvider();
        $this->vehicleService->saveVehicleSearchHistory($dataProvider);

        $this->cookieService->removeSearchCookies();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Vehicle models.
     * @return mixed
     */
    public function actionGuestIndex()
    {
        $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'showAll', 'showOwn', true);
        if (!$this->isCurrentUserHasActive()) {
            Yii::$app->getSession()->setFlash('danger', static::MESSAGE_VEHICLE_REGISTRATION_IS_SUSPENDED);
        }
        list($searchModel, $dataProvider) = $this->vehicleService->getGuestDataProvider();

        return $this->render('guest_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  Action for activation view modal form
     *
     * @param $id
     */
    public function actionViewForm($id)
    {
        $model = $this->vehicleService->getModelById($id);

        $this->permissionService->checkPermission(
            $this->vehicleService->getModelClassShort(),
            'showAll',
            'showOwn',
            ($this->isCurrentUserHasManagementRole()
                && $model->isUnknown()) ? true : $model->getAuthorsIds()
        );

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form_view', [
                'model' => $model,
            ]);
        }
        return $this->redirect(OutputHelper::getUrlQuery([Yii::$app->params['adminUrl'] . '/vehicle/view/', 'id' => $model->id]));
    }

    /**
     * Creates a new Vehicle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return \yii\web\Response|string|array
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'add');
        if (!$this->isCurrentUserHasActive()) {
            Yii::$app->getSession()->setFlash('danger', static::MESSAGE_VEHICLE_REGISTRATION_IS_SUSPENDED);

            return $this->redirect($this->getLayerWithSearch());
        }
        $model = $this->vehicleService->getNewVehicle();

        $post = Yii::$app->request->post();

        try {
            $post = $this->vehicleService->optimizeProvincePost($post);
            if (is_array($result = $model->ajaxValidation($post))) {
                return $result;
            }

            try {
                $this->vehicleService->checkIfVehicleTypeOverflowing($post['BuildingVehicleStatus'] ?? [], $model);
            } catch (VehicleTypeOverflowingException $exception) {
                Yii::$app->getSession()->setFlash('danger', $exception->getMessage());

                return $this->render('create', ['model' => $model,]);
            }

            try {
                $vehicle = $this->vehicleService->storeAndCreateInvoicesIfNeeded($post);
            } catch (StoreVehicleException $exception) {
                Yii::$app->getSession()->setFlash('danger', $exception->getMessage());

                return $this->redirect($this->getLayerWithSearch());
            } catch (VehicleBuildingValidationException $exception){
                Yii::$app->getSession()->setFlash('danger', $exception->getMessage());

                $postedVehicle = $post['Vehicle'];
                $model->hydrate($postedVehicle);

                return $this->render('create', ['model' => $model, 'oldData' => true]);
            }

            AlertViewHelper::showAddSuccessFlash(Yii::$app->controller->id, $vehicle);

            if (isset($post['UserPayment'])) {
                try {
                    $this->vehicleService->payVehicleRegistrationFee(
                        $vehicle->getRegistrationFeeInvoiceBills(),
                        $post['UserPayment']
                    );
                } catch (StoreVehiclePaymentException $exception) {
                    Yii::$app->getSession()->setFlash('warning', $exception->getMessage());

                    return $this->redirect(['bills/index']);
                }
            }

            $user = $vehicle->userOwner->user ?? null;
            $building = $user->firstBuilding ?? null;
            $permit_type_id = ArrayHelper::getValue($vehicle->getMyBuildingVehicleStatuses()
                ->andWhere([BuildingVehicleStatus::tableName() . '.status_id' => $vehicle->active_statuses])
                ->andWhere(['not', ['type_id' => '0']])
                ->andWhere([BuildingVehicleStatus::tableName() . '.building_id' => $building->id ?? null])
                ->one(), 'type_id');

            return $this->redirect(["parking-permit/create-wizard",
                'permitTypeId' => $permit_type_id,
                'vehicleId' => $vehicle->id,
                'userId' => $user->id ?? null,
                'buildingId' => $building->id ?? null
            ]);
        } catch (BaseException $e) {
            if (Yii::$app->request->isGet) {
                $model->created_by = User::shortClassName();
            }

            return $this->render('create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing Vehicle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->vehicleService->getModelById($id);
        $this->vehicleService->setVehicle($model);

        $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(),
            'editAll', 'editOwn',
            ($this->isCurrentUserHasManagementRole() && $this->vehicleService->isUnknown())
                ? true
                : $model->getAuthorsIds());
        if (!$this->isCurrentUserHasManagementRole() && $this->vehicleService->isConflicted()) {
            return $this->redirect(['index']);
        }
        try {
            $post = $this->vehicleService->optimizeProvincePost(Yii::$app->request->post());

            if (!$this->isCurrentUserHasManagementRole() && $this->vehicleService->hasSold()) {
                return $this->redirect($this->getLayerWithSearch());
            }
            if (is_array($result = $model->ajaxValidation(Yii::$app->request->post()))) {
                return $result;
            }

            try {
                $this->vehicleService->checkIfVehicleTypeOverflowing($post['BuildingVehicleStatus'] ?? [], $model);
            } catch (VehicleTypeOverflowingException $exception) {
                Yii::$app->getSession()->setFlash('danger', $exception->getMessage());

                return $this->render('update', ['model' => $model,]);
            }

            $this->vehicleService->setCurrentBuildingVehicleType();
            $vehicle = $this->vehicleService->save($post);
            $this->vehicleService->setSavedBuildingVehicleType();
            $this->vehicleService->deactivatePermits();

            AlertViewHelper::showUpdateSuccessFlash(Yii::$app->controller->id, $vehicle);
            return $this->redirect($this->getLayerWithSearch());
        } catch (BaseException $e) {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Action for activation create/update modal form
     */
    public function actionCreateUpdateVehicle()
    {
        try {
            $post = $this->vehicleService->optimizeProvincePost(Yii::$app->request->post());
            $model = $this->vehicleService->getModelByIdOrNew(ArrayHelper::getValue($post['Vehicle'], 'id'));
            $this->vehicleService->setVehicle($model);
            if ($model->isNewRecord) {
                $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'add');
            } else {
                $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'editAll',
                    'editOwn', $model->getAuthorsIds());
            }

            $this->vehicleService->setCurrentBuildingVehicleType();
            $vehicle = $this->vehicleService->save($post);
            $this->vehicleService->setSavedBuildingVehicleType();
            $this->vehicleService->deactivatePermits();

            $this->parkingPermitService->saveVehicleParkingPermits($vehicle, $post);

            $user = User::model($vehicle->ownersIds);
            // Shows how you can preselect a value

            $block = [
                'selected' => $vehicle->id,
                'current' => (!$vehicle->isNewRecord)
                    ? ['id' => $vehicle->id, 'name' => $vehicle->fullName]
                    : null,
                'vehicle_user_block' => $this->renderFile('@backend/views/user/registration/_part/_vehicle_part.php',
                    [
                        'vehicles' => $user->vehicles,
                        'user' => $user,
                        'form' => Yii::$app->request->isAjax ? 'ajax-edit-user-vehicle' : 'edit-user-vehicle'
                    ]),
            ];

            return Json::encode($block);
        } catch (BaseException $e) {
            return Json::encode(['error' => $model->getFirstErrors()]);
        }
    }

    /**
     * Deletes an existing Vehicle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->vehicleService->getModelById($id);

        $this->permissionService->checkPermission($this->vehicleService->getModelClassShort(), 'deleteAll');

        $this->parkingPermitService->deactivatePermitsByVehicle($model);

        $model->deleteWithRelations();
        return $this->redirect($this->getLayerWithSearch());
    }

    /**
     * Changes vehicle status
     *
     * @return string
     *
     * @throws \Exception
     */
    public function actionChangeStatus(): string
    {
        $post = Yii::$app->request->post();

        return $this->buildingVehicleStatusService->attemptChangeStatus($post);
    }

}
