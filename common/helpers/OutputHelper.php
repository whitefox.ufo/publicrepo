<?php
namespace common\helpers;

use common\models\amenity\AmenityGuest;
use common\models\amenity\AmenityReservation;
use common\models\amenity\AmenitySuspendUser;
use common\models\system\SystemLog;
use common\models\user\User;
use Yii;
use yii\helpers\{Html, ArrayHelper};
use common\models\parkingpermit\{ParkingPermit, ParkingPermitStatus, ParkingPermitType};
use yii\base\Model;
use DateTime;
use DateTimeZone;

class OutputHelper
{
    /**
     * @param common\models\parkingpermit\ParkingPermit $parkingPermit
     *
     * @return DateTime
     */
    public static function getPermitStartDateTimeUTC(ParkingPermit $parkingPermit): DateTime
    {
        $permitTimeZone = empty($parkingPermit->buildingTimeZone) ?
            $parkingPermit->timeZone :
            $parkingPermit->buildingTimeZone;

        $buildingTimeZone = new DateTimeZone($permitTimeZone);

        $permitStartTime = DateTime::createFromFormat(
            'H:i:s',
            $parkingPermit->parkingPermitTemplate->start_time,
            $buildingTimeZone
        );

        $startDateTime = DateTime::createFromFormat(
            "Y-m-d H:i",
            $parkingPermit->start_date_time,
            $buildingTimeZone
        );

        $startDateTime->setTime(
            $permitStartTime->format('H'),
            $permitStartTime->format('i')
        );

        return $startDateTime->setTimezone(new DateTimeZone('UTC'));
    }

    /**
     * Count Hours used in given date time period
     *
     * @param \DateTime $date
     * @param \DateTime $startTime
     * @param bool $fromLastDay
     *
     * @return int
     */
    public static function getHoursUsed(\DateTime $date, \DateTime $startTime, bool $fromLastDay) : int
    {
        $startingMoment = (clone $date)
            ->setTime($startTime->format('H'), $startTime->format('i'));

        if ($fromLastDay && $date<$startingMoment){
            $usedHours = abs(($startingMoment->diff($date->modify('+1 day')))->h);
        } else {
            $usedHours = abs(($startingMoment->diff($date))->h);
        }

        return $usedHours;
    }

    /**
     * Check if correction of 1 day if needed and return it
     *
     * @param ParkingPermit $parkingPermit
     *
     * @return int
     * @throws \Exception
     */
    public static function checkForDayCorrection(ParkingPermit $parkingPermit): int
    {
        $correction = 0;
        $wholeDayWorking = false;
        $parkingPermit->start_date_time = $parkingPermit->start_date_time_format;
        $permitStart = new \DateTime($parkingPermit->start_date_time);

        if ($parkingPermit->parkingPermitTemplate->use_template_time == 1) {
            $permitWorkStart = new \DateTime($parkingPermit->parkingPermitTemplate->start_time);
            $permitWorkEnd = new \DateTime($parkingPermit->parkingPermitTemplate->end_time);
        } else {
            $permitWorkStart = new \DateTime($parkingPermit->building->day_start);
            $permitWorkEnd = new \DateTime($parkingPermit->building->day_end);
        }

        $permitTimeStart = (new \DateTime($parkingPermit->start_date_time))
            ->setTime($permitWorkStart->format('H'), $permitWorkStart->format('i'));
        $permitTimeEnd = (new \DateTime($parkingPermit->start_date_time))
            ->setTime($permitWorkEnd->format('H'), $permitWorkEnd->format('i'));

        $fromLastDay = $permitWorkStart > $permitWorkEnd;

        if ($fromLastDay) {
            $permitTimeEnd->modify('+1 day');
        }

        $workHoursInterval = $permitTimeStart->diff($permitTimeEnd);

        if ($fromLastDay && $workHoursInterval->h == 23 && $workHoursInterval->i > 0) {
            $wholeDayWorking = true;
        }

        $permitWorkFormated = $permitWorkStart->format('H:i:s');
        $permitStartFormated = $permitStart->format('H:i:s');

        if (($fromLastDay && $permitStartFormated < $permitWorkEnd->format('H:i:s'))
            || ($permitWorkFormated < $permitWorkEnd->format('H:i:s')
                && $permitWorkFormated < $permitStartFormated
                && $permitWorkEnd->format('H:i:s') > $permitStartFormated)
            || (!$fromLastDay && !$wholeDayWorking
                && $permitWorkFormated < $permitWorkEnd->format('H:i:s')
                && $permitWorkFormated > $permitStartFormated)
        ) {
            $sameDayEnd = true;
        } else {
            $sameDayEnd = false;
        }

        if ($fromLastDay && $sameDayEnd) {
            $correction = 1;
        }

        return $correction;
    }

}
