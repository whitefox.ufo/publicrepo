<?php

namespace common\modules\Vehicle\Services;

use common\models\vehicle\Vehicle;

interface VehicleServiceInterface
{

    /**
     * get the list of vehicle types
     * @return array
     */
    public function getVehicleTypes(): array;

    /**
     * Store new vehicle and create invoices if needed
     *
     * @param array<string, mixed> $post
     *
     * @return \common\models\vehicle\Vehicle
     *
     * @throws \common\modules\Vehicle\Exceptions\StoreVehicleException
     * @throws \common\modules\Vehicle\Exceptions\StoreVehiclePaymentException
     * @throws \common\exceptions\VehicleBuildingValidationException
     */
    public function storeAndCreateInvoicesIfNeeded(array $post): Vehicle;

    /**
     * Used to create vehicle registration fee invoice or multiple invoices if multiple buildings require payment
     *
     * @param array<string, mixed> $post
     * @param \common\models\vehicle\Vehicle $vehicle
     *
     * @return void
     *
     * @throws \Exception
     */
    public function createVehicleRegistrationFeeInvoices(array $post, Vehicle $vehicle): void;

    /**
     * Used to pay vehicle registration fee during vehicle registration
     *
     * @param array<\common\models\user\UserBill> $userInvoiceBills
     * @param array<string, mixed> $cardDataWithAmount
     *
     * @return void
     *
     * @throws \common\modules\Vehicle\Exceptions\StoreVehiclePaymentException
     */
    public function payVehicleRegistrationFee(array $userInvoiceBills, array $cardDataWithAmount): void;

    /**
     * Check for vehicle overflow by type
     *
     * @param array<string, mixed> $buildingVehicleStatusData
     * @param \common\models\vehicle\Vehicle $vehicle
     *
     * @return void
     *
     * @throws \common\modules\BuildingVehicleStatus\Exceptions\VehicleTypeOverflowingException
     */
    public function checkIfVehicleTypeOverflowing(array $buildingVehicleStatusData, Vehicle $vehicle): void;
}
