<?php

namespace common\modules\Vehicle\Services;

use backend\modules\payments\models\PaymentType;
use common\exceptions\ModelValidationException;
use common\exceptions\VehicleBuildingValidationException;
use common\helpers\StorageHelper;
use common\models\address\Country;
use common\models\address\Province;
use common\models\building\Building;
use common\models\building\BuildingUser;
use common\models\building\BuildingVehicleStatus;
use common\models\parkingpermit\ParkingPermit;
use common\models\parkingpermit\ParkingPermitStatus;
use common\models\parkingpermit\ParkingPermitVehicles;
use common\models\search\VehicleSearch;
use common\models\user\User;
use common\models\user\UserBill;
use common\models\user\UserPayment;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleFile;
use common\models\vehicle\VehicleOwner;
use common\models\vehicle\VehicleSearchHistory;
use common\models\vehicle\VehicleType;
use common\modules\Building\Services\BuildingServiceInterface;
use common\modules\BuildingConsent\Services\BuildingConsentServiceInterface;
use common\modules\BuildingVehicleStatus\Exceptions\VehicleTypeOverflowingException;
use common\modules\BuildingVehicleStatus\Services\BuildingVehicleStatusServiceInterface;
use common\modules\Payment\Services\PaymentServiceInterface;
use common\modules\UserBill\Services\UserBillServiceInterface;
use common\modules\UserPayment\Services\UserPaymentServiceInterface;
use common\modules\Vehicle\Exceptions\StoreVehicleException;
use common\modules\Vehicle\Exceptions\StoreVehiclePaymentException;
use common\services\AbstractService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class VehicleService extends AbstractService implements VehicleServiceInterface
{
    /**
     * @var \common\models\vehicle\Vehicle
     */
    protected Vehicle $vehicle;

    /**
     * @var array
     */
    protected array $currentBuildingVehicleType;

    /**
     * @var array
     */
    protected array $savedBuildingVehicleType;

    /**
     * Error message for by type overflow
     *
     * @var string
     */
    protected const MESSAGE_TEMPLATE_VEHICLE_OVERFLOW_BY_TYPE = '<strong>Something went wrong!</strong> No free place for vehicle of this type in the selected building %s.';

    /**
     * Error message for unsuccessful vehicle fee payment
     *
     * @var string
     */
    protected const MESSAGE_TEMPLATE_VEHICLE_FEE_UNSUCCESSFUL_PAYMENT = 'Something went wrong during processing vehicle registration fee payment: %s. Please try again on the accounting page.';


    /**
     * @var BuildingConsentServiceInterface
     */
    protected BuildingVehicleStatusServiceInterface $buildingVehicleStatusService;

    /**
     * @var \common\modules\BuildingConsent\Services\BuildingConsentServiceInterface
     */
    protected BuildingConsentServiceInterface $buildingConsentService;

    /**
     * @var \common\modules\Payment\Services\PaymentServiceInterface
     */
    protected PaymentServiceInterface $paymentService;

    /**
     * @var \common\modules\UserBill\Services\UserBillServiceInterface
     */
    protected UserBillServiceInterface $userBillService;

    /**
     * @var \common\modules\UserPayment\Services\UserPaymentServiceInterface
     */
    protected UserPaymentServiceInterface $userPaymentService;

    /**
     * @var \common\modules\Building\Services\BuildingServiceInterface
     */
    protected BuildingServiceInterface $buildingService;

    public function __construct(
        BuildingVehicleStatusServiceInterface $buildingVehicleStatusService,
        BuildingConsentServiceInterface $buildingConsentService,
        PaymentServiceInterface         $paymentService,
        UserBillServiceInterface        $userBillService,
        UserPaymentServiceInterface     $userPaymentService,
        BuildingServiceInterface $buildingService
    ){
        $this->buildingVehicleStatusService = $buildingVehicleStatusService;
        $this->buildingConsentService = $buildingConsentService;
        $this->paymentService = $paymentService;
        $this->userBillService = $userBillService;
        $this->userPaymentService = $userPaymentService;
        $this->buildingService = $buildingService;
    }

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return Vehicle::class;
    }

    /**
     * @return string
     */
    public function getModelClassShort(): string
    {
        return Vehicle::shortClassName();
    }

    /**
     * @return string
     */
    public function getSearchClass(): string
    {
        return VehicleSearch::class;
    }

    /**
     * @param array $criteria
     * @return array|null
     * @throws NotFoundHttpException
     */
    public function findOneModelByCriteria(array $criteria): ?Vehicle
    {
        $class = $this->getModelClass();
        $model = $class::find()->andWhere($criteria)->one();
        if ($model === null) {
            throw new NotFoundHttpException(static::TEMPLATE_NOT_FOUND);
        }

        return $model;
    }

    public function getNewVehicle(?string $scenario = 'insert'): Vehicle
    {
        return new Vehicle(['scenario'=>$scenario]);
    }

    /**
     * Set vehicle variable by incoming entity
     * @param Vehicle $value
     * @return void
     */
    public function setVehicle(Vehicle $value): void
    {
        $this->vehicle = $value;
    }

    /**
     * Get vehicle types in building
     * @return array
     */
    public function getBuildingVehicleTypes():array
    {
        return ArrayHelper::map($this->vehicle->buildingVehicleStatuses, 'building_id', 'type_id');
    }

    /**
     * Set the current list of vehicle building type
     * It is necessary to compare differences in vehicles' types in building
     * and deactivate vehicle permits in specific building if type changed
     * @return void
     */
    public function setCurrentBuildingVehicleType():void
    {
        $this->currentBuildingVehicleType = $this->getBuildingVehicleTypes();
    }

    /**
     * Set the  list of vehicle building type after save actions.
     * It is necessary to compare differences in vehicles' types in building
     * and deactivate vehicle permits in specific building if type changed
     * @return void
     */
    public function setSavedBuildingVehicleType():void
    {
        $this->savedBuildingVehicleType = $this->getBuildingVehicleTypes();
    }

    /**
     * Check building vehicle types and deactivate permit if a list of buildings changed
     * @return void
     */
    public function deactivatePermits(): void
    {
       $permit_changed = array_diff_assoc($this->currentBuildingVehicleType, $this->savedBuildingVehicleType);
       if($permit_changed || $this->vehicle->isNewRecord){
           return;
       }
        $rows = [];
        $permit_vehicle_row=['parking_permit_id'=>[], 'vehicle_id'=>$this->vehicle->id];
        $vehicle_permits = $this->vehicle
            ->getPermits()
            ->andWhere([ParkingPermit::tableName().'.building_id'=>array_keys($permit_changed)])
            ->all();
        foreach($vehicle_permits as $permit){
            if(!empty($permit->building->hang_tag) &&
                ParkingPermitVehicles::find()
                    ->where(['parking_permit_id'=>$permit->id])
                    ->count()>1
            ){
                $permit_vehicle_row['parking_permit_id'][] = $permit->id;
                continue;
            }
            $rows['id'][] = $permit->id;
        }
        if(!empty($rows)){
            \Yii::$app->db->createCommand()->update(ParkingPermit::tableName(),
                ['parking_permit_status_id' => ParkingPermitStatus::STATUS_CANCELED_ID],
                $rows)->execute();
        }
        if(!empty($permit_vehicle_row)){
            ParkingPermitVehicles::deleteAll($permit_vehicle_row);
        }
    }

    /**
     * @return array
     */
    public function getUnknownDataProvider(): array
    {
        $searchClass = $this->getSearchClass();
        $searchModel = new $searchClass();
        $dataProvider = $searchModel->searchUnknown(Yii::$app->request->queryParams);
        return [$searchModel, $dataProvider];
    }

    /**
     * Check the vehicle has a sold status somewhere
     * @return bool
     */
    public function hasSold():bool
    {
        return in_array(Vehicle::STATUS_SOLD,
            ArrayHelper::getColumn($this->vehicle->buildingVehicleStatuses, 'status_id'));
    }

    /**
     * Save Vehicle item.
     * @param array $data
     * @return Vehicle|null
     */
    public function save(array $data): ?Vehicle
    {
        $post_vehicle = ArrayHelper::getValue($data, 'Vehicle');
        $this->vehicle = $this->vehicle ?? $this->getNewVehicle();

        if ($this->vehicle->isNewRecord && $vehicle = $this->searchExistent($post_vehicle)) {
            $this->vehicle = $vehicle;
            $this->vehicle->ownersIds = array_merge(
                ArrayHelper::getColumn($this->vehicle->owners, 'ownerable_id'),
                ArrayHelper::getValue($post_vehicle, 'ownersIds', [])
            );
        }else {
            if (!$this->vehicle->load($data, 'Vehicle')) {
                throw new ModelValidationException(static::MESSAGE_COULD_NOT_READ_DATA);
            }
        }

        $this->conflictCheck();
        if(!$this->vehicle->isNewRecord){
            $this->specifyRegistrant();
        }

        $this->vehicle->upload_insurance_file = UploadedFile::getInstances($this->vehicle, 'upload_insurance_file');
        $this->vehicle->upload_registration_file = UploadedFile::getInstances($this->vehicle, 'upload_registration_file');

        $this->vehicle->upload();

        $this->syncRelatedTables($data);

        return  $this->vehicle;
    }

    /**
     * save and attaches files which were saved to array
     *
     * @param $files
     * @param $type
     * @param $id
     * @return void
     */
    public function saveVehicleFiles($files, $type): void
    {
        foreach ($files as $path) {
            $localPath = StorageHelper::getLocalFilePath($path);
            if (!is_file($localPath)) continue;

            try {
                $file = 'uploads/vehicle/' . $type . '/' . StringHelper::basename($path);
                $fileString = StorageHelper::directUploadFile($file, $localPath);

                $status = VehicleFile::saveFile([
                    'vehicle_id' => $this->vehicle->id,
                    'filename' => $fileString,
                    'type_id' => constant(VehicleFile::class.'::'.mb_strtoupper($type.'_type'))
                ]);
            } catch (\common\exceptions\FileNotUploadedException $e) {
            }
        }
    }

    /**
     * Check Vehicle conflict
     * @return void
     */
    public function conflictCheck(): void
    {
        if(!$this->isUnknown() && !empty($this->conflictedWith)){
            $this->vehicle->type_id = VehicleType::CONFLICTED_TYPE;
        }

    }

    /**
     * Check vehicle type is conflict
     * @return bool
     */
    public function isConflicted():bool
    {
        return $this->vehicle->type_id == VehicleType::CONFLICTED_TYPE;
    }

    /**
     * Check the vehicle type is unknown
     * @return bool
     */
    public function isUnknown()
    {
        return $this->vehicle->type_id == VehicleType::UNKNOWN_USER_TYPE && empty($this->vehicle->buildingVehicleStatuses);
    }

    /**
     * @return void
     */
    public function specifyRegistrant()
    {
        if($this->isUnknown() && $this->vehicle->specify_registrant){
            $this->vehicle->type_id = null;
        }
    }

    /**
     * Collect conflicted vehicle list with current vehicle data
     *
     * @return \common\models\building\
     */
    public  function conflictedWith()
    {
        $vehicle_id = $this->getSoldAndDeactivatedVehicles();
        if(!$this->vehicle->isNewRecord) {
            $vehicle_id[] = $this->vehicle->id;
        }
        $criteria = ['and'];
        $criteria[] = !empty($vehicle_id)?['not',['id'=> $vehicle_id]]:[];
        $criteria[] = $this->getWhereProvinceCountry();
        $criteria[] = $this->getWhereLicense();
        $criteria[] = $this->getWhereVin();
        $where = array_diff($criteria, ['']);

        if(!empty($this->license) && !empty($this->vin)){
            $license_where = ['and'];
            $license_where[] = !empty($vehicle_id)?['not',['id'=> $vehicle_id]]:[];
            $license_where[] = $this->getWhereProvinceCountry();
            $license_where[] = $this->getWhereLicense();
            $license_where[] = ['type_id'=>VehicleType::UNKNOWN_USER_TYPE];
            $license_where[] = [$this->getWhereLicense()];
            $license_where = array_diff($license_where, ['']);

            $where = ['or', $where, $license_where];
        }

        return  $this->findModelByCriteria($where);
    }

    /**
     * Get list of vehicle id which were sold or deactivated
     * @return array
     */
    public function getSoldAndDeactivatedVehicles(): array
    {
        $criteria = ['and',
            [BuildingUser::tableName() . '.status_id' => User::STATUS_DELETED],
            ['not', [Vehicle::tableName() . '.id' => $this->vehicle->id ?? null]]
        ];

        $criteria[] = $this->getWhereProvinceCountry();
        $criteria[] = $this->getWhereLicense();
        $criteria = array_diff($criteria, ['']);
        $list = Vehicle::find()
            ->select(Vehicle::tableName() . '.id')->distinct()
            ->leftJoin(BuildingVehicleStatus::tableName(), BuildingVehicleStatus::tableName() . '.vehicle_id=' . Vehicle::tableName() . '.id')
            ->leftJoin(VehicleOwner::tableName(), VehicleOwner::tableName() . '.vehicle_id=' . Vehicle::tableName() . '.id')
            ->leftJoin(BuildingUser::tableName(), VehicleOwner::tableName() . '.ownerable_id=' . BuildingUser::tableName() . '.user_id')
            ->andFilterWhere($criteria)
            ->orWhere([BuildingVehicleStatus::tableName() . '.status_id' => Vehicle::STATUS_SOLD])
            ->all();
        return array_diff(ArrayHelper::getColumn($list, 'id'), ['']);
    }

    /**
     * Search exist vehicle by params
     *
     * @param $data
     * @return \common\models\building\|null
     * @throws \Exception
     */
    public function searchExistent(array $data): ?Vehicle
    {
        $table_name = Vehicle::tableName();
        $criteria =['and',
            [
                $table_name.'.province_id'=> \common\helpers\ArrayHelper::getValue($data,'province_id')
            ],
            [
                $table_name.'.country_id'=>ArrayHelper::getValue($data,'country_id')
            ],
            ['or',['<','type_id',VehicleType::UNKNOWN_USER_TYPE],'isnull(type_id)']
        ];

        if (!empty($license=ArrayHelper::getValue($data,'license'))){
            $criteria[] = ['like',$table_name.'.license', $license, false];
        }
        if (!empty($vin = ArrayHelper::getValue($data, 'vin'))){
            $criteria[] = ['like',$table_name.'.vin', $vin, false];
        }
        try {
            $vehicle = $this->findOneModelByCriteria($criteria);
        }catch(\Exception $e){
            return null;
        }
        return $vehicle;
    }

    /**
     * @return array
     */
    public function getGuestDataProvider(): array
    {
        $searchClass = $this->getSearchClass();
        $searchModel = new $searchClass();
        $method = $this->isPermission($this->getModelClassShort(), 'showAll') ?
            'search'  :
            'searchOwn';

        $dataProvider = $searchModel->$method(Yii::$app->request->queryParams);

        $prefix = $dataProvider->query->getNormalizePrefix();
        $dataProvider->query->andWhere([$prefix['_'] . 'buildings.type_id' => VehicleType::VISITOR_TYPE]);
        $dataProvider->query->joinWith(['vehicleOwners vehicleOwner']);
        $dataProvider->query->innerJoin(BuildingVehicleStatus::tableName() . ' guest_index_status_table',
            'guest_index_status_table.vehicle_owner_id=vehicleOwner.id');
        $dataProvider->query->andFilterWhere(['vehicleOwner.ownerable_id' => Yii::$app->user->id]);

        return [$searchModel, $dataProvider];
    }

    /**
     * Save vehicle search history from index page requests
     *
     * @param $dataProvider
     * @return void
     * @throws \Exception
     */
    public function saveVehicleSearchHistory($dataProvider):void
    {
        $search = ArrayHelper::getValue(Yii::$app->request->queryParams, 'VehicleSearch');

        if (!array_intersect(array_keys($search??[]), ['license', 'vin', 'permit'])) {
            return;
        }
        $vehicles = $dataProvider->query->all();
        $vehicle = 0;
        $meta = ['url' => Yii::$app->request->absoluteUrl, 'params' => $search];

        if (count($vehicles) == 1) {
            $vehicle = reset($vehicles);
        }
        VehicleSearchHistory::saveHistory(['VehicleSearchHistory' => [
            'parking_permit_id' => !empty($permit) ? $permit->id : 0,
            'vehicle_id' => !empty($vehicle) ? $vehicle->id : 0,
            'comment' => json_encode($meta),
            'user_id' => Yii::$app->user->id,
        ]]);

    }

    /**
     * Get the list of vehicles' type
     * @return array
     */
    public function getVehicleTypes(): array
    {
        return ArrayHelper::map(VehicleType::find()->andWhere(['not',['parking_permit_type_id' => 0]])->all(), 'id', 'name');
    }

    /**
     * {@inheritDoc}
     */
    public function storeAndCreateInvoicesIfNeeded(array $post): Vehicle
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $vehicle = $this->save($post);
        } catch (ModelValidationException $exception) {
            $transaction->rollBack();

            throw new StoreVehicleException();
        } catch (VehicleBuildingValidationException $exception){
            $transaction->rollBack();

            throw $exception;
        }

        if (isset($post['UserPayment'])) {
            try {
                $this->createVehicleRegistrationFeeInvoices($post, $vehicle);
            } catch (\Exception $exception) {
                $transaction->rollBack();

                throw new StoreVehicleException();
            }
        }

        $transaction->commit();

        return $vehicle;
    }

    /**
     * {@inheritDoc}
     */
    public function createVehicleRegistrationFeeInvoices(array $post, Vehicle $vehicle): void
    {
        $userId = array_keys($post['BuildingVehicleStatus'])[0];
        $buildingIdsWhichRequirePayment = array_keys(array_filter($post['UserPayment'], function ($value) {
            return is_array($value) && isset($value['amount']) && $value['amount'] != 0;
        }));

        $post['UserPayment']['entity'] = get_class($vehicle);
        $post['UserPayment']['value'] = $vehicle->id;
        $post['UserPayment']['user_id'] = $userId;
        $post['UserPayment']['description'] = UserBill::DESCRIPTION_VEHICLE_REGISTRATION_FEE_INVOICE;
        $post['UserPayment']['type_id'] = PaymentType::INVOICE;
        $post['UserPayment']['author_id'] = Yii::$app->user->id;
        $post['UserPayment']['sign'] = '-';
        $post['UserPayment']['card_num'] = UserPayment::EMPTY_CARD_NUM;

        foreach ($buildingIdsWhichRequirePayment as $buildingId) {
            //create an invoice bill record
            $post['UserPayment']['amount'] = $post['UserPayment'][$buildingId]['amount'];
            $post['UserPayment']['building_id'] = $buildingId;
            $invoiceUserBill = $this->userBillService->create($post, true);

            //create an invoice user_payment record
            $post['UserPayment']['user_bill_id'] = $invoiceUserBill->id;
            $post['UserPayment']['created_at'] = $invoiceUserBill->created_at;
            $this->userPaymentService->load($post, 'insert', true);
            $this->userPaymentService->create();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function payVehicleRegistrationFee(array $userInvoiceBills, array $cardDataWithAmount): void
    {
        $totalPaymentAmount = $cardDataWithAmount['amount'];

        $paymentData = ['UserPayment' => $cardDataWithAmount];
        $paymentData['UserPayment']['type_id'] = PaymentType::CREDIT_CARD;
        $paymentData['UserPayment']['status'] = UserBill::STATUS_PAID;
        $paymentData['UserPayment']['author_id'] = Yii::$app->user->id;
        $paymentData['UserPayment']['user_id'] = Yii::$app->user->id;

        $transaction = Yii::$app->db->beginTransaction();

        $i = -1;
        try {
            foreach ($userInvoiceBills as $userInvoiceBill) {
                $i++;

                $paymentData['UserPayment']['building_id'] = $userInvoiceBill->building_id;
                $paymentData['UserPayment']['user_bill_id'] = $userInvoiceBill->id;
                $paymentData['UserPayment']['description'] = UserBill::DESCRIPTION_VEHICLE_REGISTRATION_FEE_PAYMENT;
                $paymentData['UserPayment']['amount'] = $userInvoiceBill->amount;

                $this->paymentService = $this->userBillService->getPaymentService(
                    $userInvoiceBill,
                    $this->userBillService->getBillPaymentTypeAccount($userInvoiceBill)
                );
                $serviceFee = $this->paymentService->getPaymentServiceFee(
                    $this->buildingService->getPaymentFeeType($userInvoiceBill->building)
                );

                $this->userPaymentService->getNewUserPaymentByBill($userInvoiceBill->id, 'no_cash');
                $this->userPaymentService->setSignType();
				
				//since we process only one card transaction, we need this to calculate service fee correctly
				$userPaymentAmount = $paymentData['UserPayment']['amount'];
				$paymentData['UserPayment']['amount'] = $totalPaymentAmount;
                $paymentData['UserPayment']['serviceFee'] = $this->userPaymentService->countServiceFee(
                    $paymentData,
                    $serviceFee
                );
				$paymentData['UserPayment']['amount'] = $userPaymentAmount;
                $paymentBill = $this->userBillService->create($paymentData, true);

                $paymentData['UserPayment']['user_bill_id'] = $paymentBill->id;
                $this->userPaymentService->load($paymentData,  'insert', true);
                if ($i === 0) {
                    $this->userPaymentService->userPayment->amount = $totalPaymentAmount;
                    $this->userPaymentService->cardTransaction();
                    $this->userPaymentService->userPayment->amount = $userPaymentAmount;
                } else {
                    $this->userPaymentService->userPayment->card_num = $payment->getOldAttribute('card_num');
                    $this->userPaymentService->userPayment->transact_id = $payment->getOldAttribute('transact_id');
                    $this->userPaymentService->userPayment->auth_code = $payment->getOldAttribute('auth_code');
                    $this->userPaymentService->userPayment->sign = $payment->getOldAttribute('sign');
                }
                $payment = $this->userPaymentService->create();
                $this->userPaymentService->createBillsPartialPayment([$userInvoiceBill]);
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();

            foreach ($userInvoiceBills as $userInvoiceBill) {
                $this->buildingVehicleStatusService->simplyUpdateStatusByVehicleIdAndBuildingId(
                    (int)$userInvoiceBill->value,
                    (int)$userInvoiceBill->building_id,
                    Vehicle::STATUS_PENDING
                );
            }

            throw new StoreVehiclePaymentException(sprintf(static::MESSAGE_TEMPLATE_VEHICLE_FEE_UNSUCCESSFUL_PAYMENT, $exception->getMessage()));
        }

        $transaction->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function checkIfVehicleTypeOverflowing(array $buildingVehicleStatusData, Vehicle $vehicle): void
    {
        foreach ($buildingVehicleStatusData as $userId => $buildingVehicleStatuses)
        {
            foreach ($buildingVehicleStatuses as $buildingId => $buildingVehicleStatus)
            {
                $building = Building::findOne($buildingId);
                $typeId = $buildingVehicleStatus['type_id'] ?? null;
                if (empty($typeId)) {
                    continue;
                }
                $vehiclesSelectionMethod = ($building->per_registrant_unit == 0) ?
                    'countVehicleActiveStatusPerUser' : 'countVehicleActiveStatusPerUnit';
                $existingVehicles = $this->buildingVehicleStatusService->$vehiclesSelectionMethod(
                    null,
                    $building->id,
                    $typeId,
                    $userId,
                );
                $allowedVehicles = $building
                    ->additionalAllowedActive[array_search($buildingVehicleStatus['type_id'], VehicleType::namedTypes())]['value'] ?? null;
                if (is_null($allowedVehicles)) {
                    continue;
                }

                if (!$vehicle->isNewRecord)
                {
                    $existingVehicleStatus = BuildingVehicleStatus::find()
                        ->andWhere(['building_id' =>  $building->id, 'vehicle_id'=> $vehicle['id']])
                        ->one();

                    if (!empty($existingVehicleStatus) &&
                        ($existingVehicleStatus->statusName == BuildingVehicleStatus::STATUS_ACTIVE
                            || $existingVehicleStatus->statusName == BuildingVehicleStatus::STATUS_PENDING))
                    {
                        $existingVehicles --;
                    }
                }

                if ($allowedVehicles <= $existingVehicles)
                {
                    throw new VehicleTypeOverflowingException(
                        sprintf(static::MESSAGE_TEMPLATE_VEHICLE_OVERFLOW_BY_TYPE, $building->name)
                    );
                }
            }
        }
    }
}
