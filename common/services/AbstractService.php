<?php

namespace common\services;

use common\components\traits\Permissions;
use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

abstract class AbstractService
{
    use Permissions;

    /**
     * @var string
     */
    protected const MESSAGE_COULD_NOT_READ_DATA = 'Could not read data.';

    /**
     * @var string
     */
    protected const MESSAGE_VEHICLE_TYPE_OVERFLOW = 'You have no free space for this vehicle';

    /**
     * @var string
     */
    protected const TEMPLATE_NOT_FOUND = 'The requested page does not exist.';

    /**
     * @return string
     */
    abstract public function getModelClass(): string;

    /**
     * @return string
     */
    abstract public function getModelClassShort(): string;

    /**
     * @return string
     */
    abstract public function getSearchClass(): string;

    /**
     * @return array
     */
    public function getDataProvider(): array
    {
        $searchClass = $this->getSearchClass();
        $searchModel = new $searchClass();
        $method = $this->isPermission($this->getModelClassShort(), 'showAll') ?
            'search'  :
            'searchOwn';

        $dataProvider = $searchModel->$method(Yii::$app->request->queryParams);

        return [$searchModel, $dataProvider];
    }

    /**
     * Tries to get model based on its primary key value.
     *
     * @param string $id
     *
     * @return yii\db\ActiveRecord|null
     */
    public function findModelById(string $id)
    {
        $class = $this->getModelClass();

        return $class::findOne($id);
    }

    /**
     * Finds  model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return yii\db\ActiveRecord the loaded model
     *
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    public function getModelById(string $id): ActiveRecord
    {
        $class = $this->getModelClass();
        $model = $class::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException(static::TEMPLATE_NOT_FOUND);
        }

        return $model;
    }

    /**
     * Finds  model based on its primary key value.
     * If the model is not found, it returns a new entity.
     *
     * @param string $id
     *
     * @return yii\db\ActiveRecord the loaded model
     *
     */
    public function getModelByIdOrNew(string $id)
    {
        $class = $this->getModelClass();
        $model = $class::findOne($id);
        if ($model === null) {
            $model = new $class();
        }

        return $model;
    }

    /**
     * Finds  model based on values list placed on criteria.
     * If the model is not found, it returns a new entity.
     *
     * @param string $id
     *
     * @return yii\db\ActiveRecord the loaded model
     *
     */
    public function findOneModelByCriteriaOrNew(array $criteria)
    {
        $class = $this->getModelClass();
        $model = $class::find()->andWhere($criteria)->one();
        if ($model === null) {
            $model = new $class();
        }

        return $model;
    }

    /**
     * @param array $criteria
     * @return array|null
     * @throws NotFoundHttpException
     */
    public function findOneModelByCriteria(array $criteria)
    {
        $class = $this->getModelClass();
        $model = $class::find()->andWhere($criteria)->one();
        if ($model === null) {
            throw new NotFoundHttpException(static::TEMPLATE_NOT_FOUND);
        }

        return $model;
    }

    /**
     * Finds models based on its name-value pairs.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param array $criteria
     *
     * @return mixed
     *
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModelByCriteria(array $criteria)
    {
        $class = $this->getModelClass();
        $model = $class::findAll($criteria);
        if ($model === null) {
            throw new NotFoundHttpException(static::TEMPLATE_NOT_FOUND);
        }

        return $model;
    }

    /**
     * Set a Warning type flash message with prefilled message
     * @param string $message
     * @return void
     */
    public function warningFlash(string $message):void
    {
        Yii::$app->getSession()->setFlash('warning', $message);
    }

    /**
     * Set a Success type flash message with prefilled message
     * @param string $message
     * @return void
     */
    public function successFlash(string $message):void
    {
        Yii::$app->getSession()->setFlash('success', $message);
    }

    /**
     * Set a Danger type flash message with prefilled message
     * @param string $message
     * @return void
     */
    public function dangerFlash(string $message):void
    {
        Yii::$app->getSession()->setFlash('danger', $message);
    }
}
